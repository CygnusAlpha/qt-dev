#!/bin/sh
set -e

echo "$(date) - Ready.."
echo "Pwd: `pwd`"
echo "Run: $@"
exec "$@"
